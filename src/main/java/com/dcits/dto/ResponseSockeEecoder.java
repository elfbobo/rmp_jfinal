package com.dcits.dto;

import com.alibaba.fastjson.JSON;

import javax.websocket.DecodeException;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;

/**
 * description: todo
 *
 * @author sun jun
 * @className ResponseSocketDTO
 * @date 2020/9/21 15:31
 */
public class ResponseSockeEecoder implements javax.websocket.Encoder.Text<ResponseSocketDTO> {


    @Override
    public String encode(ResponseSocketDTO responseSocketDTO) throws EncodeException {
        return JSON.toJSONString(responseSocketDTO);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
