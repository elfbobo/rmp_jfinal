package com.dcits.business.server.linux.constant;

import java.util.Map;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/7 17:10
 */
public class UserCustomLinuxCommand {
    private Map<String, String> linuxCommand;
    private Map<String, String> sunCommand;
    private Map<String, String> hpCommand;

    public static UserCustomLinuxCommand defaultSettingInstance() {
        UserCustomLinuxCommand customLinuxCommand = new UserCustomLinuxCommand();
        customLinuxCommand.linuxCommand = CommandConstant.LINUX_COMMAND_MAP;
        customLinuxCommand.hpCommand = CommandConstant.HP_COMMAND_MAP;
        customLinuxCommand.sunCommand = CommandConstant.SUN_COMMAND_MAP;

        return customLinuxCommand;
    }


    public Map<String, String> getLinuxCommand() {
        return linuxCommand;
    }

    public void setLinuxCommand(Map<String, String> linuxCommand) {
        this.linuxCommand = linuxCommand;
    }

    public Map<String, String> getSunCommand() {
        return sunCommand;
    }

    public void setSunCommand(Map<String, String> sunCommand) {
        this.sunCommand = sunCommand;
    }

    public Map<String, String> getHpCommand() {
        return hpCommand;
    }

    public void setHpCommand(Map<String, String> hpCommand) {
        this.hpCommand = hpCommand;
    }
}
