package com.dcits.mvc.common.validator;

import java.util.regex.Pattern;

import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantGlobalAttributeName;
import com.dcits.constant.ConstantReturnCode;
import com.dcits.dto.RenderJSONBean;
import com.dcits.mvc.common.model.UserConfig;
import com.dcits.mvc.common.service.UserConfigService;
import com.dcits.tool.StringUtils;
import com.jfinal.core.Controller;
import com.jfinal.validate.ValidateException;
import com.jfinal.validate.Validator;

public class UserKeyValidator extends Validator {
	
	private static UserConfigService userConfigService = new UserConfigService();
	private String errorMsg = "参数验证不通过";
	private int errorCode = ConstantReturnCode.VALIDATE_FAIL;
	
	public void validate(Controller controller) {
		String userKey = controller.getPara("userKey");		
		String actionKey = getActionKey();
		//验证userKey
		if (actionKey.equals("/config/add")) {
			String password = controller.getPara("password");
			if (!StringUtils.regExpVali(Pattern.compile("^[A-Za-z0-9_]{6,20}$"), userKey)) {
				invalid = true;
				errorMsg = "用户名不符合要求:6-20位字母数字组合!";
				throw new ValidateException();
			}
			if (!StringUtils.regExpVali(Pattern.compile("^[A-Za-z0-9_]{6,20}$"), password)) {
				invalid = true;
				errorMsg = "密码不符合要求:6-20位字母数字组合!";
				throw new ValidateException();
			}
			if (userConfigService.findByKey(userKey) != null) {
				invalid = true;
				errorMsg = "用户名已存在!";
				throw new ValidateException();
			}
		} else {
			UserConfig config = controller.getSessionAttr(ConstantGlobalAttributeName.LOGIN_USER);
			if (config == null || !config.getUserKey().equals(userKey)) {
				invalid = true;
				errorMsg = "未登录或登录已失效!";
				errorCode = ConstantReturnCode.SESSION_INVALID;
				throw new ValidateException();
			}
			if (UserSpace.getUserSpace(userKey) == null) {
				UserSpace.addUserSpace(config);
			}
		}		
	}
	
	public void handleError(Controller controller) {
		RenderJSONBean json = new RenderJSONBean();
		json.setReturnCode(errorCode);
		json.setMsg(errorMsg);
		controller.renderJson(json);
	}
}
